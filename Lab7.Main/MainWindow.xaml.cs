﻿using Lab7.AddressControl.Contract;
using Lab7.Infrastructure;
using Lab7.RemoteImageControl.Contract;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Lab7.Main
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            var container = Configuration.ConfigureApp();

            var address = container.Resolve<IAddress>();
            Grid.SetRow(address.Control, 0);
            var rdef = new RowDefinition();
            rdef.Height = new GridLength(60);
            this.Panel.RowDefinitions.Add(rdef);
            this.Panel.Children.Add(address.Control);

            for (int i = 0; i < 3; i++)
            {
                var image = container.Resolve<IRemoteImage>();
                Grid.SetRow(image.Control, i+1);
                this.Panel.RowDefinitions.Add(new RowDefinition());
                this.Panel.Children.Add(image.Control);
                image.EventDownloadStart += image_DownloadingStarted;
                image.EventDownloadEnd += image_DownloadingFinished;
            }

            this.Panel.ShowGridLines = true;
        }

        void image_DownloadingFinished(object sender, EventArgs e)
        {
            global::System.Windows.Forms.MessageBox.Show("Download End!");
        }

        void image_DownloadingStarted(object sender, EventArgs e)
        {
            global::System.Windows.Forms.MessageBox.Show("Download Start!");
        }

        
    }
}
