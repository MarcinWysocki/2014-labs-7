﻿using Lab7.AddressControl.Contract;
using Lab7.RemoteImageControl.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace Lab7.RemoteImageControl.Implementation
{
    public class RemoteImageControlAdapter : IRemoteImage
    {
        private RemoteImageControl view;
        private RemoteImageControlViewModel viewModel;
        private IAddress address;

        public event EventHandler EventDownloadStart;
        public event EventHandler EventDownloadEnd;

        public RemoteImageControlAdapter(IAddress address)
        {
            this.address = address;
            this.view = new RemoteImageControl();
            this.viewModel = new RemoteImageControlViewModel();

            this.view.DataContext = this.viewModel;
            this.address.AddressChanged += address_AddressChanged;
        }

        public async void address_AddressChanged(object sender, AddressChangedArgs e)
        {
            if (e == null)
            { 
                throw new ArgumentException("!!!");
            }
            string link = e.URL;
            await LoadAsync(link);
        }

        private void DownloadStart()
        {
            if (EventDownloadStart != null)
            {
                EventDownloadStart(this, EventArgs.Empty);
            }
        }
        private void DownloadEnd()
        {
            if (EventDownloadEnd != null)
            {
                EventDownloadEnd(this, EventArgs.Empty);
            }
                
        }

        public Control Control
        {
            get { return view; }
        }
        public void Load(string url)
        {
            DownloadStart();
            viewModel.ImageSource = Sciagnij(url);
            DownloadEnd();
        }
        public async Task LoadAsync(string url)
        {
            Task<ImageSource> urlele = SciagnijAsync(url);    
            DownloadStart();
            viewModel.ImageSource = await urlele;
            DownloadEnd();
        }  

        public ImageSource Sciagnij(string url)
        {
            WebClient wc = new WebClient();
            byte[] bytes;
            try
            {
                bytes = wc.DownloadData(url);
            }
            catch (Exception)
            {
                return null;
            }
            var ms = new MemoryStream(bytes);

            ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
            return (ImageSource)imageSourceConverter.ConvertFrom(ms);
        }
        public async Task<ImageSource> SciagnijAsync(string url)
        {
            ImageSource imageSource = null;
            await Task.Run(() => { imageSource = Sciagnij(url); });
            return imageSource;
        }
    }
}