﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Windows.Media;

namespace Lab7.RemoteImageControl.Implementation
{
    internal class RemoteImageControlViewModel : INotifyPropertyChanged
    {
        private ImageSource imageSource;

        public event PropertyChangedEventHandler PropertyChanged;

        public ImageSource ImageSource
        {
            get { return this.imageSource; }
            set
            {
                if (value != imageSource)
                {
                    imageSource = value;
                    OnProperyChanged(GetPropertyChangedEventArgs());
                }
            }
        }
        private void OnProperyChanged(PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs == null)
            {
                throw new ArgumentException("!!!"); 
            }
            if (PropertyChanged != null)
            {
                PropertyChanged(this, propertyChangedEventArgs);
            }
              
        }
        public PropertyChangedEventArgs GetPropertyChangedEventArgs()
        {
            Expression<Func<RemoteImageControlViewModel, object>> propertyExpression = 
                remoteImageViewModel => remoteImageViewModel.imageSource;

            MemberExpression memberExpression = propertyExpression.Body as MemberExpression;
            string propName = memberExpression == null ? null : memberExpression.Member.Name;
            return new PropertyChangedEventArgs(propName);
        }
    }
}
