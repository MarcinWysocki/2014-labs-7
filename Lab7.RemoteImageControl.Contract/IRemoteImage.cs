﻿using Lab7.AddressControl.Contract;
using System;
using System.Windows.Controls;

namespace Lab7.RemoteImageControl.Contract
{
    public interface IRemoteImage
    {
        event EventHandler EventDownloadStart;
        event EventHandler EventDownloadEnd;

        Control Control { get; }
        void Load(string url);
    }
}
