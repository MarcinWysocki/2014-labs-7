﻿using System;

namespace Lab7.AddressControl.Contract
{

    public class AddressChangedArgs : EventArgs
    {
        private string url;
        public string URL { get { return this.url; } set { this.url = value; } }
    }
}
