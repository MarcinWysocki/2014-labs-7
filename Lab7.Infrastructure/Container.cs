﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;
using System.Reflection;

namespace Lab7.Infrastructure
{
    public class Container : IContainer
    {
        private IDictionary<Type, Type> słownikTyp = new Dictionary<Type, Type>();
        private IDictionary<Type, Object> słownikObiekt = new Dictionary<Type, Object>();

        public void Register<T>(Func<T> provider) where T : class
        {
            Register(provider.Invoke() as T);
        }
        public void Register<T>(T impl) where T : class
        {
            var interfejsy = impl.GetType().GetInterfaces();
            ////
            foreach (var item in interfejsy)
            {
                if (!słownikObiekt.ContainsKey(item))
                {
                    słownikObiekt.Add(item, impl);
                }

                else
                {
                    słownikObiekt[item] = impl;
                }

            }
        }
        public void Register(Type type)
        {

            ////////
            for (int i = 0; i < type.GetInterfaces().Length; i++)
            {
                słownikTyp[type.GetInterfaces()[i]] = type;
            }


        }
        public void Register(System.Reflection.Assembly assembly)
        {
            Type[] TypLokalny = new Type[] { };
            TypLokalny = assembly.GetTypes();

            foreach (var i in TypLokalny)
            {
                if (i.IsPublic)
                {
                    this.Register(i);
                }
            }
        }
        public object Resolve(Type type)
        {
            ///
            if (słownikObiekt.ContainsKey(type))
            {
                return słownikObiekt[type];
            }

            else if (!słownikTyp.ContainsKey(type))
            {
                return null;
            }

            else
            {
                ConstructorInfo[] tablicaKonstrukt = słownikTyp[type].GetConstructors();

                foreach (ConstructorInfo item in tablicaKonstrukt)
                {

                    var parInfo = item.GetParameters();

                    if (parInfo.Length == 0)
                    {
                        return Activator.CreateInstance(słownikTyp[type]);
                    }

                    object[] tablicaObj = new object[parInfo.Length];

                    foreach (var paramInfo in parInfo)
                    {
                        object obiekt = Resolve(paramInfo.ParameterType);

                        if (obiekt == null)
                        {
                            throw new UnresolvedDependenciesException("Brak zależności!");
                        }

                        for (int i = 0; i < tablicaObj.Length; i++)
                        {
                            tablicaObj[i] = obiekt;
                        }
                    }

                    return item.Invoke(tablicaObj);
                }
            }

            return null;
        }
        public T Resolve<T>() where T : class
        {
            return (T)Resolve(typeof(T));
        }
    }
}
