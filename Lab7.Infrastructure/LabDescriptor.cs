﻿using Lab7.AddressControl.Contract;
using Lab7.RemoteImageControl.Contract;
using Lab7.RemoteImageControl.Implementation;
using PK.Container;
using System;
using System.Reflection;
using Lab7.AddressControl;

namespace Lab7.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Func<IContainer> ContainerFactory = () => new Container();

        public static Assembly AddressControlSpec = Assembly.GetAssembly(typeof(IAddress));
        public static Type AddressImpl = typeof(AddressControlAdapter);
        public static Assembly AddressControlImpl = Assembly.GetAssembly(AddressImpl);

        public static Assembly RemoteImageControlSpec = Assembly.GetAssembly(typeof(IRemoteImage));
        public static Assembly RemoteImageControlImpl = Assembly.GetAssembly(typeof(RemoteImageControlAdapter));
        
        #endregion
    }
}
