﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TinyMVVMHelper;

namespace Lab7.AddressControl
{
    internal class AddressControlViewModel : INotifyPropertyChanged
    {
        private string adres;
        private Command confirm;
        public event PropertyChangedEventHandler PropertyChanged;

        public string Adres
        {
            get { return adres; }
            set
            {
                adres = value;
                OnPropertyChanged();
            }
        }
      
        public Command Confirm
        {
            get { return confirm; }
            set
            {
                confirm = value;
                OnPropertyChanged();
            }
        }

        public void Confirm_Method(object o){}
        
        public AddressControlViewModel()
        {
            adres = "";
            confirm = new Command(Confirm_Method);
        }
     
        public void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
