﻿using Lab7.AddressControl.Contract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7.AddressControl
{
    public class AddressControlAdapter : IAddress
    {
        private AddressControl adressControl;

        public AddressControlAdapter()
        {
            this.adressControl = new AddressControl();
            (this.adressControl.DataContext as AddressControlViewModel).PropertyChanged += AddressControlAdapter_PropertyChanged;
        }

        private void AddressControlAdapter_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Adres")
            {
                OnAddressChanged();
            }
        }

        private void OnAddressChanged()
        {
            if (AddressChanged != null)
            {
                AddressChanged(this, new AddressChangedArgs()
                { URL = (adressControl.DataContext as AddressControlViewModel).Adres, });
            }
        }

        public System.Windows.Controls.Control Control
        {
            get { return adressControl; }
        }

        public event EventHandler<AddressChangedArgs> AddressChanged;

        public void OnAdressChasnged()
        {
            if (AddressChanged != null)
            {
                AddressChanged(this, new AddressChangedArgs());
            }
        }
    }
}
